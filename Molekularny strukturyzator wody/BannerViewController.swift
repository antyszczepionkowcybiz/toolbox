//
//  BannerViewController.swift
//  Molekularny strukturyzator wody
//
//  Created by Paweł Burda on 27/02/2019.
//  Copyright © 2019 Antyszczepionkowcy.biz. All rights reserved.
//

import UIKit

final class BannerViewController: UIViewController {

    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var textImageView: UIImageView!
    
    private let premiereDate = Date(timeIntervalSince1970: 1552698000)
    private var isAfterPremiere: Bool {
        return Date() > premiereDate
    }
    private var actionURL = URL(string: "https://antyszczepionkowcy.biz/")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainButton.layer.shadowColor = UIColor(named: "MainBlue")?.cgColor
        mainButton.layer.shadowOpacity = 0.8
        mainButton.layer.shadowRadius = 10
        mainButton.layer.shadowOffset = .zero

        if isAfterPremiere {
            mainButton.setImage(UIImage(named: "KupTerazButton"), for: .normal)
            actionURL = URL(string: "https://polakpotrafi.pl/projekt/antyszczepionkowcybiz")!
            textImageView.image = UIImage(named: "TextAfter")
        }
        
        applyMotionEffect(toView: mainButton, magnitude: 20)
    }
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func mainButtonAction(_ sender: UIButton) {
        UIApplication.shared.open(actionURL, options: [:], completionHandler: nil)
    }
    
    private func applyMotionEffect(toView view: UIView, magnitude: Float) {
        let xMotion = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        xMotion.minimumRelativeValue = -magnitude
        xMotion.maximumRelativeValue = magnitude
        
        let yMotion = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        yMotion.minimumRelativeValue = -magnitude
        yMotion.maximumRelativeValue = magnitude
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [xMotion, yMotion]
        
        view.addMotionEffect(group)
    }
    
}
