//
//  ViewController.swift
//  Molekularny strukturyzator wody
//
//  Created by Paweł Burda on 24/11/2018.
//  Copyright © 2018 Antyszczepionkowcy.biz. All rights reserved.
//

import UIKit
import BAFluidView
import AVKit

class ViewController: UIViewController {

    @IBOutlet weak var procedureButton: UIButton!
    
    private var player: AVAudioPlayer?
    private var torchTimer: Timer?
    
    private lazy var fluidView: BAFluidView = {
        let fluidView = BAFluidView(frame: view.frame)
        fluidView.fillColor = UIColor(named: "MainBlue")
        fluidView.fillAutoReverse = false
        fluidView.fillRepeatCount = 1
        return fluidView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        preparePlayer()
        view.insertSubview(fluidView, belowSubview: procedureButton)
        procedureButton.layer.cornerRadius = 10
    }

    @IBAction func procedureAction(_ sender: UIButton) {
        fluidView.fillDuration = 6
        fluidView.fill(to: 1.0)
        procedureButton.isEnabled = false
        UIView.transition(with: procedureButton, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.procedureButton.setTitle("Strukturyzacja w toku", for: .normal)
        }, completion: nil)
        
        player?.play()
        
        torchTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { [weak self] _ in
            self?.toggleTorch()
        }
        delay(fluidView.fillDuration) { [weak self] in
            self?.resetState()
            self?.showSuccessAlert()
        }
    }
    
    private func showSuccessAlert() {
        let alert = UIAlertController(title: "Sukces!", message: "Molekularna strukturyzacja wody przebiegła pomyślnie.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { [unowned self] _ in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "BannerViewController")
            self.present(vc, animated: true, completion: nil)
        }
        alert.addAction(okAction)
        alert.preferredAction = okAction
        show(alert, sender: nil)
    }
    
    private func resetState() {
        torchTimer?.invalidate()
        toggleTorch(on: false)
        fluidView.fillDuration = 1
        fluidView.fill(to: 0)
        procedureButton.isEnabled = true
        UIView.transition(with: procedureButton, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.procedureButton.setTitle("Rozpocznij procedurę", for: .normal)
        }, completion: nil)
    }
    
    private func preparePlayer() {
        guard let asset = NSDataAsset(name: "Blip") else {
            return
        }
        do {
            player = try AVAudioPlayer(data: asset.data, fileTypeHint: "aiff")
            player?.numberOfLoops = 1
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func toggleTorch(on: Bool? = nil) {
        guard let captureDevice = AVCaptureDevice.default(for: .video) else {
            return
        }
        if captureDevice.isTorchAvailable && captureDevice.isTorchModeSupported(.on) {
            do {
                try captureDevice.lockForConfiguration()
                captureDevice.torchMode = on ?? (captureDevice.torchMode == .off) ? .on : .off
                captureDevice.unlockForConfiguration()
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
}

