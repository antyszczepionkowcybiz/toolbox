//
//  Helpers.swift
//  Molekularny strukturyzator wody
//
//  Created by Paweł Burda on 24/11/2018.
//  Copyright © 2018 Antyszczepionkowcy.biz. All rights reserved.
//

import Foundation

func delay(_ delay: Double, closure: @escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}
