//
//  AppDelegate.swift
//  Molekularny strukturyzator wody
//
//  Created by Paweł Burda on 24/11/2018.
//  Copyright © 2018 Antyszczepionkowcy.biz. All rights reserved.
//

import UIKit
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        do {
            let audioSession = AVAudioSession.sharedInstance()
            try audioSession.setCategory(.playback, mode: .default)
            try audioSession.setActive(true)
        } catch {
            print("error: \(error.localizedDescription)")
        }
        
        return true
    }

}

